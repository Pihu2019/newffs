//
//  NotesTableViewCell.h
//  The Future Foundation School
//
//  Created by shayam on 15/06/19.
//  Copyright © 2019 Akhilesh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *createdDate;
@property (weak, nonatomic) IBOutlet UILabel *file;
@property (weak, nonatomic) IBOutlet UIButton *downloadBtn;
- (IBAction)downloadBtnClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *descriptionNotes;

@end

NS_ASSUME_NONNULL_END
