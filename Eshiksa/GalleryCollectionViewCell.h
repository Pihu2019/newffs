
#import <UIKit/UIKit.h>

@interface GalleryCollectionViewCell : UICollectionViewCell
@property(nonatomic,weak)IBOutlet UIImageView *galleryImgView;

@property (weak, nonatomic) IBOutlet UILabel *foldername;
@property (weak, nonatomic) IBOutlet UILabel *folderid;
@property (weak, nonatomic) IBOutlet UILabel *imageid;
@end
