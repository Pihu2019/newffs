
#import "StudentHomeworkVC.h"
#import "StudentHomework.h"
#import "Constant.h"
#import "StudentHomeworkTableViewCell.h"
#import "BaseViewController.h"
#import "Base.h"
#import "WebViewController.h"
#import "PDFKBasicPDFViewer.h"
#import "PDFKDocument.h"
#import "MBProgressHUD.h"

@interface StudentHomeworkVC ()
{
    NSArray *filePathsArray;
    NSString *newpdfString;
}
@end

@implementation StudentHomeworkVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableview setSeparatorColor:[UIColor clearColor]];
    _homeworkArr=[[NSMutableArray alloc]init];
    _homeworkNameArr=[[NSMutableArray alloc]init];
    _subjectNameArr=[[NSMutableArray alloc]init];
    _submissionDateArr=[[NSMutableArray alloc]init];
    _homeworkPathArr=[[NSMutableArray alloc]init];
    
    _tableview.delegate=self;
    _tableview.dataSource=self;
    _downloadView.hidden=YES;
//    [self parsingHomeworkData];
//}
//-(void)parsingHomeworkData
//{
    UIActivityIndicatorView *indicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.frame=CGRectMake(self.view.window.center.x,self.view.window.center.y, 40.0, 40.0);
    indicator.center=self.view.center;
    [self.view addSubview:indicator];
    indicator.tintColor=[UIColor redColor];
    indicator.backgroundColor=[UIColor lightGrayColor];
    [indicator bringSubviewToFront:self.view];
    [indicator startAnimating];
    
    [_homeworkArr removeAllObjects];
    [_homeworkNameArr removeAllObjects];
    [_subjectNameArr removeAllObjects];
    [_submissionDateArr removeAllObjects];
    [_homeworkPathArr removeAllObjects];
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"group name in circular==%@",groupname);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"circular username ==%@",username);
    
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    NSLog(@"circular password ==%@",password);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"cyear"];
    NSLog(@"circular cyear ==%@",cyear);
    
    NSString *orgid = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"orgid"];
    NSLog(@"circular orgid ==%@",orgid);
    
     NSString *urlstr=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:homework]];
    
       NSDictionary *parameterDict = @{
                                @"groupname":groupname,
                                    @"username":username,
                                    @"instUrl":instUrl,
                                    @"dbname":dbname,
                                    @"Branch_id":branchid,
                                    @"tag":@"homework"
                                    };
    
    NSLog(@"parameterDict data:%@",parameterDict);
    
    [Constant executequery:urlstr strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        
        NSLog(@"response data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            
            NSLog(@"response  homework_list data:%@",maindic);
            
            _tag=[maindic objectForKey:@"tag"];
            _success=[[maindic objectForKey:@"success"]stringValue];
            _error=[[maindic objectForKey:@"error"]stringValue];
            _homeworkList=[maindic objectForKey:@"homework_list"];
            
            NSLog(@"tag==%@& success=%@  _homeworkList=%@",_tag,_success,_homeworkList);
            
            NSArray *ciculararr=[maindic objectForKey:@"homework_list"];
        
            NSLog(@"homework_list:%@",ciculararr);
            
            if(ciculararr.count==0)
            {
                
                _noSchedule = [[UITextView alloc]initWithFrame:
                               CGRectMake(80, 200, 400, 300)];
                [_noSchedule setText:@"No homework is given..."];
                [_noSchedule setTextColor:[UIColor grayColor]];
                [_noSchedule setFont:[UIFont fontWithName:@"ArialMT" size:16]];
                _noSchedule.delegate =self;
                [self.view addSubview:_noSchedule];
            
            
            }
            else {

            for(NSDictionary *temp in ciculararr)
            {
                NSString *str1=[[temp objectForKey:@"subject"]description];
                NSString *str2=[[temp objectForKey:@"submission_date"]description];
                NSString *str3=[temp objectForKey:@"hw_name"];
                NSString *str4=[temp objectForKey:@"hw_file"];
                
                
                NSLog(@"subject=%@  submission_date=%@ hw_name=%@ hw_file=%@",str1,str2,str3,str4);
                
                
                StudentHomework *k1=[[StudentHomework alloc]init];
                k1.subjectNameStr=str1;
                k1.submissionDateStr=str2;
                k1.homeworkStr=str3;
                k1.homeworkFileStr=str4;
                
                
                [self.homeworkArr addObject:k1];
            }
                 [self.tableview reloadData];
     
            }
        }
        [self.tableview performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableview reloadData];
            
            [indicator stopAnimating];
        });
    }]; 
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _homeworkArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    StudentHomeworkTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    StudentHomework *ktemp=[_homeworkArr objectAtIndex:indexPath.row];
    
    cell.submissionDate.text=ktemp.submissionDateStr;
    cell.subject.text=ktemp.subjectNameStr;
    cell.homeworkName.text=ktemp.homeworkStr;
    cell.homeworkFile.text=ktemp.homeworkFileStr;
    
    [cell.paidInvoiceBtn addTarget:self action:@selector(pdfButtonclicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.paidInvoiceBtn.tag=indexPath.row;
    [cell.paidInvoiceBtn setTag:indexPath.row];
    
    
//    NSString *str = [homeworkdownloadUrl stringByAppendingString:ktemp.homeworkpathStr];
//    cell.homeworkPath.text=str;
//
//    NSLog(@"homeworkPath URL in cell %@",str);
//
//    [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"homeworkPath"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return cell;
}
-(void)pdfButtonclicked:(UIButton*)sender{
    
    UIButton *btn =(UIButton *)sender;
    NSLog(@"Btn Click...........%ld",(long)btn.tag);
    indexBtn=sender.tag;
    NSLog(@"INDEX ===%ld",indexBtn);
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud.mode = MBProgressHUDAnimationFade;
    hud.labelText = @"Downloading...";
    
    [self savePdfInDocumentsDirectory];
    
}
-(NSData *)savePdfInDocumentsDirectory
{
    StudentHomework *ktemp=[_homeworkArr objectAtIndex:indexBtn];
    
    _pdfPath=[NSString stringWithFormat:@"%@",[homeworkdownloadUrl stringByAppendingString:ktemp.homeworkFileStr]];
    NSLog(@"***str3***%@",_pdfPath);
    
    [[NSUserDefaults standardUserDefaults] setObject:_pdfPath forKey:@"pdfPath"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    NSURL *finalpdfURL = [NSURL URLWithString:[_pdfPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    NSLog(@"***finalURLString***%@",finalpdfURL);
    
    NSData *pdfdata = [NSData dataWithContentsOfURL:finalpdfURL];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsPath = [paths objectAtIndex:[_indxp intValue]];
    
    filePath = [documentsPath stringByAppendingPathComponent:[[_pdfPath componentsSeparatedByString:@"/"]lastObject]];
    NSLog(@"**filePath %@",filePath);
    
    [pdfdata writeToFile:filePath atomically:YES];
    
    _downloadView.hidden=NO;
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    printf("pdf file == %s",[filePath UTF8String]);
    
    
    UISaveVideoAtPathToSavedPhotosAlbum (filePath,self,@selector(pdf:didFinishSavingWithError:contextInfo:),nil);
    
    return pdfdata;
    
    
}
- (void)pdf:(NSString *)pdfPath didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    NSLog(@"Finished saving pdf with error: %@", error);
    
}
-(void)fetchAudioInDocumentsDirectory:(NSString*)filepath
{
    data = [NSData dataWithContentsOfFile:filePath];
    
}
- (IBAction)viewDownloadBtnClciked:(UIButton*)sender {
    
    _downloadView.hidden=YES;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:sender.tag];
    
    filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    
    NSLog(@"filePathsArray===%@ document directory=%@", filePathsArray,documentsDirectory);
    
    [self fetchAudioInDocumentsDirectory:filePath];
    
    [self performSegueWithIdentifier:@"showPDFOffline3" sender:self];
    
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if ([segue.identifier isEqualToString:@"showPDFOffline3"]) {
        
        PDFKBasicPDFViewer *viewer = (PDFKBasicPDFViewer *)segue.destinationViewController;
        viewer.enableBookmarks = YES;
        viewer.enableOpening = YES;
        viewer.enablePrinting = YES;
        viewer.enableSharing = YES;
        viewer.enableThumbnailSlider = YES;
        
        PDFKDocument *document = [PDFKDocument documentWithContentsOfFile:filePath password:nil];
        NSLog(@"document==%@",document);
        
        [viewer loadDocument:document];
        
    }
}


- (void)viewDidLayoutSubviews{
    NSString *language = [@"" currentLanguage];
    if ([language isEqualToString:@"hi"])
    {
        [self setBackButtonLocalize];
    }
}

- (void)setBackButtonLocalize{
    self.navigationItem.title = [@"HOMEWORK" localize];
}
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
//{
//    StudentHomeworkTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
//     NSLog(@"cell==%@",cell);
//    _indxp=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
//
//    NSLog(@"indexpath==%ld",(long)indexPath.row);
//
//    [self performSegueWithIdentifier:@"showStudentHomeworkDownload"
//                              sender:[self.tableview cellForRowAtIndexPath:indexPath]];
//}
//
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//
//    WebViewController *wvc=[segue destinationViewController];
//    if ([segue.identifier isEqualToString:@"showStudentHomeworkDownload"]) {
//
//        NSString *homeworkPath = [[NSUserDefaults standardUserDefaults]stringForKey:@"homeworkPath"];
//        NSLog(@"***homeworkPath ==%@",homeworkPath);
//
//        wvc.myURL=homeworkPath;
//
//        NSLog(@"*******full downloading url  str=%@",homeworkPath);
//
//    }
//}

@end
