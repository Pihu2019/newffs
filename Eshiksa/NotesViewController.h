//
//  NotesViewController.h
//  The Future Foundation School
//
//  Created by shayam on 15/06/19.
//  Copyright © 2019 Akhilesh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLSessionDelegate,UITextViewDelegate>
{
    dispatch_queue_t queue;
    NSInteger indexBtn;
    NSData *data;
    NSString *filePath;
}
@property (weak, nonatomic) IBOutlet UITableView *tableviewNotes;
@property (nonatomic,strong) NSMutableArray *createdDateArr,*titleArr,*descriptionArr,*fileArr,*notesArr;
@property(nonatomic,retain)NSString *indxp,*tag,*success,*error;
@property (weak, nonatomic) IBOutlet UIView *downloadView;
@end

NS_ASSUME_NONNULL_END
