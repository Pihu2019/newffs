
#import <UIKit/UIKit.h>

@interface GalleryViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;

@property (nonatomic,strong) NSMutableArray *galleryArr,*foldernameArr,*imgIdArr,*folderIdArr;
@property(nonatomic,retain)NSString *indxp,*folderIdStr,*titleStr,*isComeFrom;

@end
