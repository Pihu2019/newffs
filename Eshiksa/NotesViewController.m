//
//  NotesViewController.m
//  The Future Foundation School
//
//  Created by shayam on 15/06/19.
//  Copyright © 2019 Akhilesh. All rights reserved.


#import "NotesViewController.h"
#import "Base.h"
#import "NotesTableViewCell.h"
#import "BaseViewController.h"
#import "Constant.h"
#import "PDFKBasicPDFViewer.h"
#import "PDFKDocument.h"
#import "MBProgressHUD.h"
#import "Notes.h"
@interface NotesViewController ()
{
     NSArray *filePathsArray;
}
@end

@implementation NotesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    _notesArr=[[NSMutableArray alloc]init];
    _titleArr=[[NSMutableArray alloc]init];
    _createdDateArr=[[NSMutableArray alloc]init];
    _descriptionArr=[[NSMutableArray alloc]init];
    
    _tableviewNotes.delegate=self;
    _tableviewNotes.dataSource=self;
    
    _downloadView.hidden=YES;
    
    UIActivityIndicatorView *indicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
  indicator.frame=CGRectMake(self.view.window.center.x,self.view.window.center.y, 40.0, 40.0);
    indicator.center=self.view.center;
    [self.view addSubview:indicator];
    
    
    indicator.tintColor=[UIColor redColor];
    indicator.backgroundColor=[UIColor lightGrayColor];
    [indicator bringSubviewToFront:self.view];
    // [UIApplication sharedApplication].networkActivityIndicatorVisible=true;
    [indicator startAnimating];
    
    [_descriptionArr removeAllObjects];
    [_notesArr removeAllObjects];
    [_createdDateArr removeAllObjects];
    [_descriptionArr removeAllObjects];
    
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"group name in circular==%@",groupname);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"circular username ==%@",username);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"cyear"];
    NSLog(@"circular cyear ==%@",cyear);
    
    
    NSString *urlstr=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:notesList]];
      NSLog(@"urlstr cyear ==%@",urlstr);
    
    NSDictionary *parameterDict = @{
                                    @"groupname":groupname,
                                    @"username":username,
                                    @"instUrl":instUrl,
                                    @"dbname":dbname,
                                    @"Branch_id":branchid,
                                    @"cyear":cyear,
                                    @"notes_id":@"246",
                                    @"tag":@"my_notes"
                                    };
    
       NSLog(@"parameterDict  ==%@",parameterDict);
    [Constant executequery:urlstr strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            
            NSLog(@"response  gatepass data:%@",maindic);
            
            _tag=[maindic objectForKey:@"tag"];
            _success=[[maindic objectForKey:@"success"]stringValue];
            _error=[[maindic objectForKey:@"error"]stringValue];
           
            
            NSLog(@"Tag===%@",_tag);
            
            
            NSArray *ciculararr=[maindic objectForKey:@"notesDetail"];
            
            
            NSLog(@"notesDetail:%@",ciculararr);
            
            if(ciculararr.count==0)
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"Currently there is no notes data availbale." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alertView dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
                
                
                [alertView addAction:ok];
                
                [self presentViewController:alertView animated:YES completion:nil];
                
            }
            else {
                
                
                for(NSDictionary *temp in ciculararr)
                {
                    NSString *str1=[[temp objectForKey:@"title"]description];
                    NSString *str2=[[temp objectForKey:@"description"]description];
                    NSString *str3=[temp objectForKey:@"created_date"];
                    NSString *str4=[temp objectForKey:@"file"];
                    
                    NSLog(@"from_date=%@  to_date=%@ created_date=%@ status=%@",str1,str2,str3,str4);
                    
                    Notes *n=[[Notes alloc]init];
                    n.titleStr=str1;
                    n.descriptionStr=str2;
                    n.createdDateStr=str3;
                    n.fileStr=str4;
                    
                    
                    
                    [self.notesArr addObject:n];
                    NSLog(@"_notesArr ARRAYY%@",self->_notesArr);
                }
                [self.tableviewNotes reloadData];
            }
        }
        [self.tableviewNotes performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableviewNotes reloadData];
            
            [indicator stopAnimating];
        });
    }];
    
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _notesArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotesTableViewCell *cell = [_tableviewNotes dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    Notes *ktemp=[_notesArr objectAtIndex:indexPath.row];
    cell.createdDate.text=ktemp.createdDateStr;
    cell.title.text=ktemp.titleStr;
    cell.descriptionNotes.text=ktemp.descriptionStr;
    cell.file.text=ktemp.fileStr;
    
    [cell.downloadBtn addTarget:self action:@selector(pdfButtonclicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.downloadBtn.tag=indexPath.row;
    [cell.downloadBtn setTag:indexPath.row];

    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 100.0f;
    
}
-(void)pdfButtonclicked:(UIButton*)sender{
    
    UIButton *btn =(UIButton *)sender;
    NSLog(@"Btn Click...........%ld",(long)btn.tag);
    indexBtn=sender.tag;
    NSLog(@"INDEX ===%ld",indexBtn);
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud.mode = MBProgressHUDAnimationFade;
    hud.labelText = @"Downloading...";
    
     [self savePdfInDocumentsDirectory];
    
}
-(NSData *)savePdfInDocumentsDirectory
{
     Notes *ktemp=[_notesArr objectAtIndex:indexBtn];
    

    NSURL *finalpdfURL = [NSURL URLWithString:[ktemp.fileStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    NSLog(@"***finalURLString***%@",finalpdfURL);
    
    NSData *pdfdata = [NSData dataWithContentsOfURL:finalpdfURL];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsPath = [paths objectAtIndex:[_indxp intValue]];
    
    filePath = [documentsPath stringByAppendingPathComponent:[[ktemp.fileStr componentsSeparatedByString:@"/"]lastObject]];
    NSLog(@"**filePath %@",filePath);
    
    [pdfdata writeToFile:filePath atomically:YES];
    
    _downloadView.hidden=NO;
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    printf("pdf file == %s",[filePath UTF8String]);
    
    
    UISaveVideoAtPathToSavedPhotosAlbum (filePath,self,@selector(pdf:didFinishSavingWithError:contextInfo:),nil);
    
    return pdfdata;
    
    
}
- (void)pdf:(NSString *)pdfPath didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    NSLog(@"Finished saving pdf with error: %@", error);
    
}
-(void)fetchAudioInDocumentsDirectory:(NSString*)filepath
{
    data = [NSData dataWithContentsOfFile:filePath];
    
}
- (IBAction)viewDownloadBtnClciked:(UIButton*)sender {
    
    _downloadView.hidden=YES;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:sender.tag];
    
    filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    
    NSLog(@"filePathsArray===%@ document directory=%@", filePathsArray,documentsDirectory);
    
    [self fetchAudioInDocumentsDirectory:filePath];
    
    [self performSegueWithIdentifier:@"showOffline" sender:self];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if ([segue.identifier isEqualToString:@"showOffline"]) {
        
        PDFKBasicPDFViewer *viewer = (PDFKBasicPDFViewer *)segue.destinationViewController;
        viewer.enableBookmarks = YES;
        viewer.enableOpening = YES;
        viewer.enablePrinting = YES;
        viewer.enableSharing = YES;
        viewer.enableThumbnailSlider = YES;
        
        PDFKDocument *document = [PDFKDocument documentWithContentsOfFile:filePath password:nil];
        NSLog(@"document==%@",document);
        
        [viewer loadDocument:document];
        
    }
}

/*
 
 marker.position=CLLocationCoordinate2DMake([self->_latitudeStr doubleValue],[self->_longitudeStr doubleValue]);
 // marker.position=CLLocationCoordinate2DMake(21.1450677,79.0889168);
 marker.map=self.mapView;
 marker.title=self.stopNameStr;
 marker.snippet=self.addressStr;
 marker.appearAnimation=kGMSMarkerAnimationPop;
 // marker.icon=[GMSMarker markerImageWithColor:[UIColor redColor]];
 marker.icon=[UIImage imageNamed:@"map_car_running.png"];
 
 GMSMutablePath *path = [GMSMutablePath path];
 [path addCoordinate:CLLocationCoordinate2DMake([self->_latitudeStr doubleValue],[self->_longitudeStr doubleValue])];
 
 NSString *newlatitudeStr = [[NSUserDefaults standardUserDefaults] stringForKey:@"newlatitudeStr"];
 NSLog(@"newlatitudeStr ==%@",newlatitudeStr);
 
 NSString *newlongitudeStr = [[NSUserDefaults standardUserDefaults] stringForKey:@"newlongitudeStr"];
 NSLog(@"newlongitudeStr ==%@",newlongitudeStr);
 
 [path addCoordinate:CLLocationCoordinate2DMake([newlatitudeStr doubleValue],[newlongitudeStr doubleValue])];
 
 */
@end
