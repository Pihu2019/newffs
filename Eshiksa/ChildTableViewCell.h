
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChildTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *admissionNum;
@property (weak, nonatomic) IBOutlet UILabel *firstName;
@property (weak, nonatomic) IBOutlet UILabel *branchId;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UILabel *lastname;
@property (weak, nonatomic) IBOutlet UILabel *mobile;
@property (weak, nonatomic) IBOutlet UILabel *opyear;
@property (weak, nonatomic) IBOutlet UILabel *orgId;
@property (weak, nonatomic) IBOutlet UILabel *picId;
@property (weak, nonatomic) IBOutlet UILabel *studentId;
@property (weak, nonatomic) IBOutlet UILabel *transportLicence;

@end

NS_ASSUME_NONNULL_END
