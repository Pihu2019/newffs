
#import "SubscribeViewController.h"
#import "BaseViewController.h"
#import "Base.h"
#import "Subscription.h"
@interface SubscribeViewController ()

@end

@implementation SubscribeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getEtrackiAmount];
    
    self.subscribeBtn.layer.masksToBounds=YES;
    self.subscribeBtn.layer.cornerRadius=8;
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_subscribeView.bounds];
    _subscribeView.layer.masksToBounds = NO;
    _subscribeView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _subscribeView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    _subscribeView.layer.shadowOpacity = 0.5f;
    _subscribeView.layer.shadowPath = shadowPath.CGPath;
}

-(void)getEtrackiAmount{
    
    NSString *admissionNum = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"admissionNum"];
    NSLog(@"admissionNum ==%@",admissionNum);
    
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
  
    NSString *mystr=[NSString stringWithFormat:@"dbname=%@&Branch_id=%@&admission_no=%@&tag=getEtrackiDetails",dbname,branchid,admissionNum];
    
    NSLog(@"parameterDict in currnt route list%@",mystr);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];

   NSString *mainstr1=[NSString stringWithFormat:@"http://eps.eshiksa.net/esh/plugins/APIS/etracki_payment.php"];
    
    NSURL *url = [NSURL URLWithString:mainstr1];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[mystr dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if(error)
    {
    }
    NSURLSessionDataTask *dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         if(error)
                                         {
                                         }
                                         
                                         NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                         NSError *er=nil;
                                         NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                         if(er)
                                         {
                                         }
                                         NSLog(@"responseDict:%@",responseDict);
                                         
                                         NSDictionary *dic=[responseDict objectForKey:@"tracking_details"];
                                         NSLog(@"tracking_details dic%@",dic);
                                         
                                         
                                         Subscription *c=[[Subscription alloc]init];
                                         
                                         c.etracki_fees=[dic objectForKey:@"etracki_fees"];
                                         c.maxNoOfBuses=[dic objectForKey:@"maxNoOfBuses"];
                                         c.licenceActivationDate=[dic objectForKey:@"licenceActivationDate"];
                                         c.licenceDeactivationDate=[dic objectForKey:@"licenceDeactivationDate"];
                                        
                                        
                                         NSLog(@" c.etracki_fees==%@ c.maxNoOfBuses ==%@", c.etracki_fees,c.maxNoOfBuses);
                                         
                                         if (c.etracki_fees == nil || [c.etracki_fees isEqual:[NSNull null]]){
                                             c.etracki_fees=@"0";
                                         }
                                         else{
                                        
                                        self.amount.text=c.etracki_fees;
                                             
                                             NSLog(@"etraki fees=%@",self.amount.text);
                                         }
                                         
                                         if (c.maxNoOfBuses == nil || [c.maxNoOfBuses isEqual:[NSNull null]]) {
                                             c.maxNoOfBuses=@"0";
                                         }
                                         else{
                                              NSLog(@"buses==");
                                         }
                                         if (c.licenceActivationDate == nil || [c.licenceActivationDate isEqual:[NSNull null]]){
                                             c.licenceActivationDate=@"0";
                                         }
                                         else{
                                             NSLog(@" Etracki_fees==");
                                         }
                                         
                                         if (c.licenceDeactivationDate == nil || [c.licenceDeactivationDate isEqual:[NSNull null]]) {
                                             c.licenceDeactivationDate=@"0";
                                         }
                                         else{
                                             NSLog(@"buses==");
                                         }
                                    [[NSUserDefaults standardUserDefaults] setObject:c.etracki_fees forKey:@"etrackiFees"];
                                         [[NSUserDefaults standardUserDefaults] synchronize];
                                         
                                         
                                         
                                     }];
    
    [dataTask resume];
    
}
- (IBAction)subscribeBtnClicked:(id)sender {
    [self getEtrackiSubcription];
}



-(void)getEtrackiSubcription{

    NSString *admissionNum = [[NSUserDefaults standardUserDefaults]stringForKey:@"admissionNum"];
    NSLog(@"admissionNum ==%@",admissionNum);
    
    NSString *etrackiFees = [[NSUserDefaults standardUserDefaults]stringForKey:@"etrackiFees"];
    NSLog(@"etrackiFees ==%@",etrackiFees);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
    NSString *mystr=[NSString stringWithFormat:@"dbname=%@&Branch_id=%@&admission_no=%@&amount=%@&tag=etracki_pay",dbname,branchid,admissionNum,etrackiFees];
    
    NSLog(@"parameterDict in currnt route list%@",mystr);
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSString *mainstr1=[NSString stringWithFormat:@"http://eps.eshiksa.net/esh/plugins/APIS/etracki_payment.php"];
    
    NSURL *url = [NSURL URLWithString:mainstr1];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[mystr dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if(error)
    {
    }
    NSURLSessionDataTask *dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         if(error)
                                         {
                                         }
                                         
                                         NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                         NSError *er=nil;
                                         NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                         if(er)
                                         {
                                         }
                                         NSLog(@"responseDict:%@",responseDict);
                                         
                                         self.tag=[responseDict objectForKey:@"tag"];
                                         self.success=[responseDict objectForKey:@"success"];
                                         self.error=[responseDict objectForKey:@"error"];
                                         
                                         NSLog(@"tag==%@ & success=%@",self.tag,self.success);
                                         
                                         
                                         NSDictionary *dic=[responseDict objectForKey:@"response"];
                                         NSLog(@"student etracki fees dic%@",dic);
                                         
                                         NSString *txnId=[[dic objectForKey:@"txnid"]description];
                                         NSString *studentId=[[dic objectForKey:@"student_id"]description];
                                         
                                         
                                         NSLog(@"txnid==%@ student_id ==%@",txnId,studentId);
                                         
                                         if(!(self.success == nil || [self.success isEqual:[NSNull null]]))
                                         {
                                             NSLog(@"Subscribed succesffullyy");
                                    
                                             
                                          /*
                                                 String url = subscribeUrl + "esh/index.php?plugin=payment&action=tracki_index&student_id="
                                                 + response1.getStudentId() + "&txnid=" + response1.getTxnid()
                                                 + "&amount=" + trackingDetails.getEtrackiFees()
                                                 + "&branch_id=" + db.getUserDetails().getBranchId();
                                                 Intent intent = new Intent(SubscribeActivity.this, WebViewActivity.class);*/
                                             
                                         }
                                         else
                                         {
                                             NSLog(@"Unable to subscribe.");
                                         }
                                       
                                         
                                         
                                     }];
    
    [dataTask resume];

}
-(void)getEtrackiSubcription1{
    
    NSString *admissionNum = [[NSUserDefaults standardUserDefaults]stringForKey:@"admissionNum"];
    NSLog(@"admissionNum ==%@",admissionNum);
    
    NSString *etrackiFees = [[NSUserDefaults standardUserDefaults]stringForKey:@"etrackiFees"];
    NSLog(@"etrackiFees ==%@",etrackiFees);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
    
    NSString *mystr=[NSString stringWithFormat:@"dbname=%@&Branch_id=%@&admission_no=%@&amount=%@&tag=etracki_pay",dbname,branchid,admissionNum,etrackiFees];
    
    NSLog(@"parameterDict in currnt route list%@",mystr);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    //NSURL * url = [NSURL URLWithString:@"http://erp.eshiksa.net/eps_trunk/eps/esh/plugins/APIS/etracki_payment.php"];
    
    NSString *mainstr1=[NSString stringWithFormat:@"http://eps.eshiksa.net/esh/plugins/APIS/etracki_payment.php"];
    
    NSURL *url = [NSURL URLWithString:mainstr1];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[mystr dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if(error)
    {
    }
    NSURLSessionDataTask *dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         if(error)
                                         {
                                         }
                                         
                                         NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                         NSError *er=nil;
                                         NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                         if(er)
                                         {
                                         }
                                         NSLog(@"responseDict:%@",responseDict);
                                         
                                         _tag=[responseDict objectForKey:@"tag"];
                                         _success=[responseDict objectForKey:@"success"];
                                         _error=[responseDict objectForKey:@"error"];
                                         
                                         NSLog(@"tag==%@ & success=%@",_tag,_success);
                                         
                                         NSDictionary *dic=[responseDict objectForKey:@"response"];
                                         
                                         NSLog(@"response dic%@",dic);
                                         
                                         
                                         Subscription *c=[[Subscription alloc]init];
                                         
                                         c.txnId=[dic objectForKey:@"txnid"];
                                         c.studentId=[dic objectForKey:@"student_id"];
                                         
                                         
                                         NSLog(@"txnid==%@ student_id ==%@", c.etracki_fees,c.maxNoOfBuses);
                                         
                                         
                                         
                                     }];
    
    [dataTask resume];
    
}


@end
