

#import <Foundation/Foundation.h>

@interface PayFees : NSObject

@property (nonatomic,strong) NSString *fees_nameStr,*fees_amountStr,*due_dateStr,*due_amountStr,*paid_amountStr,*total_concession_amountStr,*head_fine_amountStr,*courseIdStr,*batchIdStr,*departmentIdStr,*sessionIdStr,*studentIdStr,*feesIdStr,*onlinePayDiscStr,*totalConcessionAmtStr,*feesBaseIdStr,*feesShortOrderStr;

@end
