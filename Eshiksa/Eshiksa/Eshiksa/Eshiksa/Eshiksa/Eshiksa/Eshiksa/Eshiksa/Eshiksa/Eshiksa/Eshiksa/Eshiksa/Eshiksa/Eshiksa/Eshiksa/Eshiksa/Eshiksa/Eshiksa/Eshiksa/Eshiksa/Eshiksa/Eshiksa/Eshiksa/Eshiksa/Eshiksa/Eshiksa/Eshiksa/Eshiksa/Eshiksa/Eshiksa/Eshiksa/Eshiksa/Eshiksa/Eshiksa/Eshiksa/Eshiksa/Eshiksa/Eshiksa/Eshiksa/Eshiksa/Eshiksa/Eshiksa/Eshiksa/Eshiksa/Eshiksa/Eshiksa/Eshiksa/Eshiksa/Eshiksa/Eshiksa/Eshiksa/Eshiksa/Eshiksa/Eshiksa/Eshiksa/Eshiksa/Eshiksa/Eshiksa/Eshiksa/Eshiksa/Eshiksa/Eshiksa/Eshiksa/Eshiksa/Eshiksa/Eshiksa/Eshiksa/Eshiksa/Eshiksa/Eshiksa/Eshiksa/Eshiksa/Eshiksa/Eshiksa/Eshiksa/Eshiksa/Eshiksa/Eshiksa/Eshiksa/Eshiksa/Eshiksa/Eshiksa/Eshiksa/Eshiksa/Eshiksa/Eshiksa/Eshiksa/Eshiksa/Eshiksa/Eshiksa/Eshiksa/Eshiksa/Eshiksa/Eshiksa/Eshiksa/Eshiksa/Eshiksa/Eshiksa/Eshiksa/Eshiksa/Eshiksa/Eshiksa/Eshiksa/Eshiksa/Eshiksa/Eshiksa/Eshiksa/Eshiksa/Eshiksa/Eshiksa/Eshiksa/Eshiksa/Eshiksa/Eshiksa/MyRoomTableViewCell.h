

#import <UIKit/UIKit.h>

@interface MyRoomTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *batchName;
@property (strong, nonatomic) IBOutlet UILabel *courseName;
@property (strong, nonatomic) IBOutlet UILabel *studentName;
@property (strong, nonatomic) IBOutlet UILabel *mobile;

@end
