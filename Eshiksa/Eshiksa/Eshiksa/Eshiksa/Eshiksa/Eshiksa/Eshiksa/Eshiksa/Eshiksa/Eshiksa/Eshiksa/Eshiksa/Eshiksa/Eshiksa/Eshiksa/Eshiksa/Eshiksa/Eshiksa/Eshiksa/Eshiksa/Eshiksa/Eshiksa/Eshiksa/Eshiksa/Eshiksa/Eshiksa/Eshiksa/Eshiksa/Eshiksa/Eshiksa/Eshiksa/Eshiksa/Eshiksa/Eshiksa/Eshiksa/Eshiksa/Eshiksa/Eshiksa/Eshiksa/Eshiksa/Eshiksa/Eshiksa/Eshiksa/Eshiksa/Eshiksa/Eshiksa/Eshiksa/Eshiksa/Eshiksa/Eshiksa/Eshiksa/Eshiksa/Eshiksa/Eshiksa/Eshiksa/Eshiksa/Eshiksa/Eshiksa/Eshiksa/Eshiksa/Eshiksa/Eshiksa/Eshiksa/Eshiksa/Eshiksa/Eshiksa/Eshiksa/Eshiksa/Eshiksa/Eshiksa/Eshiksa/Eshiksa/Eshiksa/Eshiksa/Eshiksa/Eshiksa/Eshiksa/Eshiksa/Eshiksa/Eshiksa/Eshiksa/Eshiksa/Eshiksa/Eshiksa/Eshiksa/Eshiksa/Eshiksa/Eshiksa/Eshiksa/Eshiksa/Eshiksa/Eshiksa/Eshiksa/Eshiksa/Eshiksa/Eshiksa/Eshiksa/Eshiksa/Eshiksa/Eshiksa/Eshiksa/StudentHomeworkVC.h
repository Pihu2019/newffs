
#import <UIKit/UIKit.h>

@interface StudentHomeworkVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
    dispatch_queue_t queue;
    NSInteger indexBtn;
    NSData *data;
    NSString *filePath;
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic,strong) NSMutableArray *submissionDateArr,*homeworkNameArr,*subjectNameArr,*homeworkPathArr,*homeworkArr;
@property(nonatomic,retain)NSString *indxp,*pdfPath;
@property (nonatomic,strong) NSString *success,*email,*error,*tag,*homeworkList;

@property (strong, nonatomic) IBOutlet UITextView *noSchedule;

@property (weak, nonatomic) IBOutlet UIView *downloadView;

@end
