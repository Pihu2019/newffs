
#import <UIKit/UIKit.h>

@interface RoomPartnersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *hostelName;
@property (weak, nonatomic) IBOutlet UILabel *issueDate;
@property (weak, nonatomic) IBOutlet UILabel *roomNumber;
@property (weak, nonatomic) IBOutlet UILabel *roomFloor;

@end
