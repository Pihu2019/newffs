//
//  NotesTableViewCell.m
//  The Future Foundation School
//
//  Created by shayam on 15/06/19.
//  Copyright © 2019 Akhilesh. All rights reserved.
//

#import "NotesTableViewCell.h"

@implementation NotesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _downloadBtn.layer.cornerRadius=10;
    _downloadBtn.layer.masksToBounds=YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)downloadBtnClicked:(id)sender {
}
@end
