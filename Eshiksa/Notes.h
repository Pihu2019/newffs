//
//  Notes.h
//  The Future Foundation School
//
//  Created by shayam on 15/06/19.
//  Copyright © 2019 Akhilesh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Notes : NSObject

@property (nonatomic,strong) NSString *createdDateStr,*titleStr,*descriptionStr,*fileStr;
@end

NS_ASSUME_NONNULL_END
