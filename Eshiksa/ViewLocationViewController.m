
// 1 215 192 782
// y274zb

//GMSCameraPosition *camera=[GMSCameraPosition cameraWithLatitude:21.1450677 longitude:79.0889168 zoom:14 bearing:0 viewingAngle:0];
//https://stackoverflow.com/questions/13433234/sending-nsnumber-strong-to-parameter-of-incompatible-type-cllocationdegree/13433261

#import "ViewLocationViewController.h"
#import "CurrentLocation.h"
#import <GoogleMaps/GoogleMaps.h>
#import "UIKit/UIKit.h"
#import "Base.h"
#import "Transport.h"
#import "ARCarMovement-Swift.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface ViewLocationViewController ()

@property (weak, nonatomic) IBOutlet UIView *mapbackView;
@property (strong, nonatomic) ARCarMovement *moveMent;

@end

@implementation ViewLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.journeyId.text=_journeyIdStr;
    NSLog(@"LATITUDE:: %@ LONGITUDE:: %@",_latitudeStr,_longitudeStr);

    self.moveMent = [[ARCarMovement alloc]init];
    self.moveMent.delegate = self;
    
    [self getCurrentRouteList];
    
   // NSString *filePath = [[NSBundle mainBundle] pathForResource:@"coordinates" ofType:@"json"];
    //NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
  //  self.CoordinateArr = [[NSMutableArray alloc]initWithArray:[NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil]];
    
    self.oldCoordinate = CLLocationCoordinate2DMake([_latitudeStr doubleValue],[_longitudeStr doubleValue]);

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[_latitudeStr doubleValue] longitude:[_longitudeStr doubleValue] zoom:14];    // Create a GMSCameraPosition that tells the map to display the marker

    //self.oldCoordinate = CLLocationCoordinate2DMake(40.7416627,-74.0049708);
   // GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:40.7416627 longitude:-74.0049708 zoom:14];
    
    self.mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    self.mapView.myLocationEnabled = YES;
    self.mapView.delegate = self;
    self.view = self.mapView;
    
    driverMarker = [[GMSMarker alloc] init];      // Creates a marker in the center of the map.
    driverMarker.position = self.oldCoordinate;
    driverMarker.title=self.stopNameStr;
    driverMarker.snippet=self.addressStr;
    driverMarker.appearAnimation=kGMSMarkerAnimationPop;
    driverMarker.icon = [UIImage imageNamed:@"map_car_running.png"];
    driverMarker.map = self.mapView;
    
   
    self.counter = 0;  //set counter value 0
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(timerTriggered) userInfo:nil repeats:true];     //start the timer, change the interval based on your requirement

}

#pragma mark - scheduledTimerWithTimeInterval Action
-(void)timerTriggered {
    
  //  if (self.counter < self.CoordinateArr.count) {
        
        NSString *newlatitudeStr = [[NSUserDefaults standardUserDefaults] stringForKey:@"newlatitudeStr"];
        NSLog(@"newlatitudeStr ==%@",newlatitudeStr);
        
        NSString *newlongitudeStr = [[NSUserDefaults standardUserDefaults] stringForKey:@"newlongitudeStr"];
        NSLog(@"newlongitudeStr ==%@",newlongitudeStr);
        
        
    CLLocationCoordinate2D newCoordinate = CLLocationCoordinate2DMake([newlatitudeStr floatValue],[newlongitudeStr floatValue]);

      //  CLLocationCoordinate2D newCoordinate = CLLocationCoordinate2DMake([self.CoordinateArr[self.counter][@"lat"] floatValue],[self.CoordinateArr[self.counter][@"long"] floatValue]);
        
        //You need to pass the created/updating marker, old & new coordinate, mapView and bearing value from backend to turn properly. Here coordinates json files is used without new bearing value. So that bearing won't work as expected.
         
        [self.moveMent arCarMovementWithMarker:driverMarker oldCoordinate:self.oldCoordinate newCoordinate:newCoordinate mapView:self.mapView bearing:0];  //instead value 0, pass latest bearing value from backend
        
        self.oldCoordinate = newCoordinate;
        self.counter = self.counter + 1; //increase the value to get all index position from array
   // }
   // else {
      //  [self.timer invalidate];
      //  self.timer = nil;
   // }
}

#pragma mark - ARCarMovementDelegate
- (void)arCarMovementMoved:(GMSMarker * _Nonnull)Marker {
    driverMarker = Marker;
    driverMarker.map = self.mapView;
    
    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:driverMarker.position zoom:15.0f];
    [self.mapView animateWithCameraUpdate:updatedCamera];    //animation to make car icon in center of the mapview

}

-(void)getCurrentRouteList{
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"circular username ==%@",username);
    
    //NSString *mystr=[NSString stringWithFormat:@"journey_id=671&Branch_id=%@&tag=Current_Location_of_Vehicle",branchid];
    
    NSString *mystr=[NSString stringWithFormat:@"journey_id=%@&admission_no=%@&Branch_id=%@&tag=Current_Location_of_Vehicle",_journeyIdStr,username,branchid];
    
    
    NSLog(@"parameterDict in currnt route list%@",mystr);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
      // NSString *mainstr1=[NSString stringWithFormat:@"http://erp.eshiksa.net/eps_trunk/eps/esh/plugins/APIS/currentroutelist.php"]; //demo
    
      NSString *mainstr1=[NSString stringWithFormat:@"http://eps.eshiksa.net/esh/plugins/APIS/currentroutelist.php"]; //live
    
    NSURL * url = [NSURL URLWithString:mainstr1];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[mystr dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if(error)
    {
    }
    NSURLSessionDataTask *dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         if(error)
                                         {
                                         }
                                         
                                         NSString *text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                         NSLog(@"text==%@",text);
                                         NSError *er=nil;
                                         NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                         if(er)
                                         {
                                         }
                                         NSLog(@"responseDict:%@",responseDict);
                                         
                                         NSDictionary *dic=[responseDict objectForKey:@"location"];
                                         NSLog(@"location dic%@",dic);
                                         
                                         if(dic.count==0)
                                         {
                                             NSLog(@"CURRENT location STOPPED***");
                                             
                                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Oops!" message:@"No location found." preferredStyle:UIAlertControllerStyleAlert];
                                             
                                             UIAlertAction* ok = [UIAlertAction
                                                                  actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action)
                                                                  {
                                                                      [alertView dismissViewControllerAnimated:YES completion:nil];
                                                                      
                                                                  }];
                                             
                                             
                                             [alertView addAction:ok];
                                             
                                             [self presentViewController:alertView animated:YES completion:nil];
                                             
                                         }
                                         else {
                                         
                                         CurrentLocation *c=[[CurrentLocation alloc]init];
                                         
                                         c.driverIdStr=[dic objectForKey:@"driver_id"];
                                         c.busIdStr=[dic objectForKey:@"bus_id"];
                                         c.cdatetimeStr=[dic objectForKey:@"cdatetime"];
                                         c.latitudeStr=[dic objectForKey:@"latetude"];
                                         c.longitudeStr=[dic objectForKey:@"longitude"];
                                         c.angleStr=[dic objectForKey:@"angle"];
                                         c.opyearStr=[dic objectForKey:@"opyear"];
                                         c.journeyIdStr=[dic objectForKey:@"journey_id"];
                                         
                                         
                                         NSLog(@"in transport driverIdStr==%@ longitudeStr ==%@",c.driverIdStr,c.longitudeStr);
                                             
                                             [[NSUserDefaults standardUserDefaults] setObject:c.latitudeStr forKey:@"newlatitudeStr"];
                                             [[NSUserDefaults standardUserDefaults] synchronize];
                                             [[NSUserDefaults standardUserDefaults] setObject:c.longitudeStr forKey:@"newlongitudeStr"];
                                             [[NSUserDefaults standardUserDefaults] synchronize];
                                             
                                            // [_mapView animateToLocation:CLLocationCoordinate2DMake([c.latitudeStr doubleValue],[c.longitudeStr doubleValue])];
                                             
                                            //comment GMSPloyline if animate this method is used
                                         }
                                     }];
    
    [dataTask resume];
    
}

-(BOOL)prefersStatusBarHidden{
    return  YES;
}
/*

-(void)parsingTransportationData{
    
    [_transportArr removeAllObjects];
    [_locationArr removeAllObjects];
    
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"circular username ==%@",username);
    
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
    NSString *mystr=[NSString stringWithFormat:@"admission_no=%@&Branch_id=%@&tag=Stop_Route_list",username,branchid];
    
    NSLog(@"parameterDict%@",mystr);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];

    
    NSString *mainstr1=[NSString stringWithFormat:@"http://erp.eshiksa.net/eps_trunk/eps/esh/plugins/APIS/currentroutelist.php"];
    
   // NSString *mainstr1=[NSString stringWithFormat:@"http://eps.eshiksa.net/esh/plugins/APIS/currentroutelist.php"];
    

    NSURL * url = [NSURL URLWithString:mainstr1];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[mystr dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if(error)
    {
    }
    NSURLSessionDataTask *dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         if(error)
                                         {
                                         }
                                         
                                         NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                         NSLog(@"text==%@",text);
                                         NSError *er=nil;
                                         NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                         if(er)
                                         {
                                         }
                                         NSLog(@"responseDict:%@",responseDict);
                                         
                                         NSArray *routelist=[responseDict objectForKey:@"route_list"];
                                         
                                         NSLog(@"route_list:%@",routelist);
                                         
                                         if(routelist.count==0)
                                         {
                                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"No data available" preferredStyle:UIAlertControllerStyleAlert];
                                             
                                             UIAlertAction* ok = [UIAlertAction
                                                                  actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action)
                                                                  {
                                                                      [alertView dismissViewControllerAnimated:YES completion:nil];
                                                                      
                                                                  }];
                                             
                                             
                                             [alertView addAction:ok];
                                             
                                             [self presentViewController:alertView animated:YES completion:nil];
                                             
                                         }
                                         else {
                                             
                                             for(NSDictionary *temp1 in routelist)
                                             {
                                                 NSString *str1=[[temp1 objectForKey:@"route_id"]description];
                                                 NSString *str2=[[temp1 objectForKey:@"route_name"]description];
                                                 NSString *str3=[temp1 objectForKey:@"pick_drop"];
                                                 NSString *str4=[temp1 objectForKey:@"stop_id"];
                                                 NSString *str5=[[temp1 objectForKey:@"stop_name"]description];
                                                 NSString *str6=[[temp1 objectForKey:@"latitude"]description];
                                                 NSString *str7=[temp1 objectForKey:@"longitude"];
                                                 NSString *str8=[temp1 objectForKey:@"journey_details"];
                                                 
                                                 
                                                 NSLog(@"route_id=%@  route_name=%@ pick_drop=%@ stop_name=%@",str1,str2,str3,str5);
                                                 
                                                 
                                                 Transport *k1=[[Transport alloc]init];
                                                 k1.routeIdStr=str1;
                                                 k1.routeNameStr=str2;
                                                 k1.pickdropStatus=str3;
                                                 k1.stopIdStr=str4;
                                                 k1.stopNameStr=str5;
                                                 k1.latitudeStr=str6;
                                                 k1.longitudeStr=str7;
                                                 k1.journeyDetailsStr=str8;
                                                 
                                                 
                                                 NSDictionary *dic=[temp1 objectForKey:@"journey_details"];
                                                 
                                                 k1.driverIdStr=[dic objectForKey:@"driver_id"];
                                                 k1.driverNameStr=[dic objectForKey:@"driver_name"];
                                                 k1.vehicleNumStr=[dic objectForKey:@"vechile_no"];
                                                 k1.vehicleIdStr=[dic objectForKey:@"vechile_id"];
                                                 k1.vehicleNameStr=[dic objectForKey:@"vechile_name"];
                                                 k1.journeyIdStr=[dic objectForKey:@"journey_id"];
                                                 k1.routeIdStr=[dic objectForKey:@"route_id"];
                                                 k1.stopStr=[dic objectForKey:@"stops"];
                                                 
                                                 NSLog(@"driverNameStr:%@  vehicleNumStr:%@ stops%@",k1.driverNameStr,k1.vehicleNumStr,k1.stopStr);
                                                 
                                                 [self->_transportArr addObject:k1];
                                                 
                                                 
                                                 
                                                 NSArray *subarr=[temp1 objectForKey:@"stops"];
                                                 
                                                 NSLog(@"****stops arr:%@",subarr);
                                                 NSMutableDictionary *temp=[[NSMutableDictionary alloc]init];
                                                 for(temp in subarr)
                                                 {
                                                     NSString *str1=[[temp objectForKey:@"Branch_id"]description];
                                                     NSString *str2=[[temp objectForKey:@"stop_id"]description];
                                                     NSString *str3=[[temp objectForKey:@"route_id"]description];
                                                     NSString *str4=[[temp objectForKey:@"stop_name"]description];
                                                     NSString *str5=[[temp objectForKey:@"lankmark"]description];
                                                     NSString *str6=[[temp objectForKey:@"landmark_address"]description];
                                                     NSString *str7=[[temp objectForKey:@"lat"]description];
                                                     NSString *str8=[[temp objectForKey:@"landmark_lat"]description];
                                                     NSString *str9=[[temp objectForKey:@"lng"]description];
                                                     NSString *str10=[[temp objectForKey:@"landmark_lng"]description];
                                                     NSString *str11=[[temp objectForKey:@"view_order"]description];
                                                     NSString *str12=[[temp objectForKey:@"fees_amount"]description];
                                                     NSString *str13=[[temp objectForKey:@"created_date"]description];
                                                     NSString *str14=[[temp objectForKey:@"status"]description];
                                                     NSString *str15=[[temp objectForKey:@"opyear"]description];
                                                     NSString *str16=[[temp objectForKey:@"org_id"]description];
                                                     NSString *str17=[[temp objectForKey:@"address"]description];
                                                     NSString *str18=[[temp objectForKey:@"pincode"]description];
                                                     
                                                     NSLog(@"Branch_id=%@  stop_id=%@ stop_name=%@ route_id=%@ landmark=%@ landmark_address=%@  lat=%@ landmark_lat=%@ lng=%@ landmark_lng=%@ view_order=%@  fees_amount=%@ created_date=%@ status=%@ opyear=%@ org_id=%@ address=%@ pincode=%@",str1,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11,str12,str13,str14,str15,str16,str17,str18);
                                                    
                                                     [self.locationArr addObject:temp];
                                                 }
                                                 

                                                 GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
                                                 CLLocationCoordinate2D location;
                                                 for (temp in self->_locationArr)
                                                 {
                                                     location.latitude = [temp[@"lat"] floatValue];
                                                     location.longitude = [temp[@"lng"] floatValue];
                                                     // Creates a marker in the center of the map.
                                                     GMSMarker *marker = [[GMSMarker alloc] init];
                                         marker.position = CLLocationCoordinate2DMake(location.latitude, location.longitude);
                                                     bounds = [bounds includingCoordinate:marker.position];
                                                     marker.title = temp[@"stop_name"];
                                                     marker.map = self.mapView;             marker.appearAnimation=kGMSMarkerAnimationPop;
                 marker.icon=[UIImage imageNamed:@"map_car_running.png"];

                                                 }
                                                 [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
                                             }
                                             dispatch_async(dispatch_get_main_queue(), ^{

                                             });
                                             }
                    }];
    [dataTask resume];
    
}*/


/*
double lat = #your latitude#;
double lng = #your longitude#;
double latEnd = #your end latitude#;
double lngEnd = #your end longitude#;
NSString *directionsUrlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?&origin=%f,%f&destination=%f,%f&mode=driving", lat, lng, latEnd, lngEnd];
NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];


NSURLSessionDataTask *mapTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                 {
                                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                     if(error)
                                     {
                                         if(completionHandler)
                                             completionHandler(nil);
                                         return;
                                     }
                                     
                                     NSArray *routesArray = [json objectForKey:@"routes"];
                                     
                                     GMSPolyline *polyline = nil;
                                     if ([routesArray count] > 0)
                                     {
                                         NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                         NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                         NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                         GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                         polyline = [GMSPolyline polylineWithPath:path];
                                     }
                                     
                                     // run completionHandler on main thread
 










/*
 
 _mapView.delegate = self;
 _locationArr=[[NSMutableArray alloc]init];
 _transportArr=[[NSMutableArray alloc]init];
 NSLog(@"LATITUDE:: %@ LONGITUDE:: %@",_latitudeStr,_longitudeStr);
 self.journeyId.text=_journeyIdStr;
 
 [self parsingTransportationData];
 
 // subscribe to location updates
 //    [[NSNotificationCenter defaultCenter] addObserver:self
 //                                             selector:@selector(updatedLocation:)
 //                                                 name:@"newLocationNotif"
 //                                               object:nil];
 
 //GMSCameraPosition *camera=[GMSCameraPosition cameraWithLatitude:21.1450677 longitude:79.0889168 zoom:14 bearing:0 viewingAngle:0];
 GMSCameraPosition *camera=[GMSCameraPosition cameraWithLatitude:[_latitudeStr doubleValue] longitude:[_longitudeStr doubleValue] zoom:14];
 
 
 self.mapView=[GMSMapView mapWithFrame:CGRectZero camera:camera];
 _mapView.myLocationEnabled = NO;
 
 self.view = _mapView;
 
 // self.mapView=[GMSMapView mapWithFrame:self.view.bounds camera:camera];
 
 self.mapView.mapType=kGMSTypeNormal;
 //self.mapView.myLocationEnabled=YES;//to find current location
 self.mapView.settings.compassButton=YES;
 self.mapView.settings.myLocationButton=YES;
 [self.mapView setMinZoom:10 maxZoom:18];
 
 [self.mapbackView addSubview:self.mapView];
 
 dispatch_async(dispatch_get_main_queue(),
 ^{
 // [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(getCurrentRouteList) userInfo:nil repeats:YES];
 
 if (self->timer==nil) {
 self->timer=[NSTimer scheduledTimerWithTimeInterval:10.0 repeats:true block:^(NSTimer * _Nonnull timer) {
 [self getCurrentRouteList];
 NSLog(@"CURRENT ROUTE LIST");
 }];
 }
 
 GMSMarker *marker=[[GMSMarker alloc]init];
 marker.position=CLLocationCoordinate2DMake([self->_latitudeStr doubleValue],[self->_longitudeStr doubleValue]);
 // marker.position=CLLocationCoordinate2DMake(21.1450677,79.0889168);
 marker.map=self.mapView;
 marker.title=self.stopNameStr;
 marker.snippet=self.addressStr;
 marker.appearAnimation=kGMSMarkerAnimationPop;
 // marker.icon=[GMSMarker markerImageWithColor:[UIColor redColor]];
 marker.icon=[UIImage imageNamed:@"map_car_running.png"];
 GMSMutablePath *path = [GMSMutablePath path];
 [path addCoordinate:CLLocationCoordinate2DMake([self->_latitudeStr doubleValue],[self->_longitudeStr doubleValue])];
 
 NSString *newlatitudeStr = [[NSUserDefaults standardUserDefaults] stringForKey:@"newlatitudeStr"];
 NSLog(@"newlatitudeStr ==%@",newlatitudeStr);
 
 NSString *newlongitudeStr = [[NSUserDefaults standardUserDefaults] stringForKey:@"newlongitudeStr"];
 NSLog(@"newlongitudeStr ==%@",newlongitudeStr);
 
 [path addCoordinate:CLLocationCoordinate2DMake([newlatitudeStr doubleValue],[newlongitudeStr doubleValue])];
 
 //                       GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
 //                       rectangle.strokeWidth = 2.f;
 //                       rectangle.map = self.mapView;
 //                       [self.mapbackView addSubview:rectangle.map];
 
 });
 
 
 locationManager = [[CLLocationManager alloc] init];
 locationManager.delegate = self;
 locationManager.distanceFilter = kCLDistanceFilterNone;
 locationManager.desiredAccuracy = kCLLocationAccuracyBest;
 
 if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
 [locationManager requestWhenInUseAuthorization];
 }
 if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
 [locationManager requestWhenInUseAuthorization];
 [locationManager startUpdatingLocation];
 
 NSDictionary *userLoc=[[NSUserDefaults standardUserDefaults] objectForKey:@"userLocation"];
 NSLog(@"updated lat %@",[userLoc objectForKey:@"lat"]);
 NSLog(@"updated long %@",[userLoc objectForKey:@"long"]);
 
 */


@end
