

#import <UIKit/UIKit.h>

@interface PaidFeesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
    
    dispatch_queue_t queue;
    NSInteger indexBtn;
    NSData *data;
    NSString *filePath;
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic,strong) NSMutableArray *billNumberArr,*createdDateArr,*feesNameArr,*payStatusArr,*feesAmountArr,*fineAmountArr,*paidAmountArr,*paidArr;
//@property (strong, nonatomic) IBOutlet UITextView *noSchedule;
@property(nonatomic,retain)NSString *indxp,*feesRecieptStr,*billNumStr,*studentIdStr,*success,*pdfPath;;
//@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *downloadView;


@end
