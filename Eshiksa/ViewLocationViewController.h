

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "ARCarMovement-Swift.h"
@interface ViewLocationViewController : UIViewController<GMSMapViewDelegate,ARCarMovementDelegate>
{
GMSMarker *driverMarker;
}
@property(nonatomic,retain)NSString *indxp,*journeyIdStr,*stopNameStr,*latitudeStr,*longitudeStr,*addressStr;
@property (weak, nonatomic) IBOutlet UILabel *journeyId;
@property (nonatomic,strong) NSMutableArray *locationArr,*transportArr;
@property CLLocationCoordinate2D oldCoordinate;
@property (strong, nonatomic) NSMutableArray *CoordinateArr;

@property (strong, nonatomic) GMSMapView *mapView;
@property (weak, nonatomic) NSTimer *timer;
@property NSInteger counter;
@end
