
#import "PayFeesTableViewCell.h"

@implementation PayFeesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.payNowBtn.layer.masksToBounds=YES;
    self.payNowBtn.layer.cornerRadius=8;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
